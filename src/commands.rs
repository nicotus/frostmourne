use std::io::{BufReader};
use std::fs::{OpenOptions};
use std::{process, env, path, io};
use std::io::prelude::*;
use colored::*;
use serde_json::{json};
use serde::{Deserialize};

#[derive(Deserialize)]
struct Command {
    command_name: String,
    args: Vec<String>,
}

impl Command {
    fn new(command_name: String, args: &[String]) -> Self {
        Command {
            command_name: command_name,
            args: args.to_vec(),
        }
    }

    fn create_command(&self) -> Result<String, io::Error> {
        /*
        JSON data should look like this:
            command_name: String,
            args: [
                String,
                String,
                ...
            ]
        */
        let command_data = json!({
                "command_name": &self.command_name,
                //"paths": &self.paths,
                "args": &self.args
        });

        // First let's try open file and create new one if doesn't exists
        let mut f = OpenOptions::new().read(true).append(true).create(true).open(".commands_frostmourne")?;

        let mut s = serde_json::to_string(&command_data)?;
        s.push_str("\n");
        f.write_all(s.as_bytes()).expect(&"Unable to write new command".red());
        println!("{}", "Wrote command to file .commands_frostmourne".green());

        Ok(s)
    }
    
    fn exectue_command(&self) -> Result<(), io::Error> {
        /*
            println!("{}", "Starting cypress in local".blue());
            let new_path = path::Path::new("~/path/to/cypress");
            assert!(env::set_current_dir(&new_path).is_ok());
            let output = process::Command::new("npx")
                .args(&["cypress", "run", "--record", "false", &head, "--no-exit", "--config", "baseUrl=http://localhost:4000", "--spec", &spec])
                .spawn()
                .expect(&"Failed to execute cypress run command".red());
            output.stdout;
        */

        Ok(())
    }

    fn get_command(args: &[String]) -> Result<(), io::Error> {
        let f = OpenOptions::new().read(true).open(".commands_frostmourne")?;
        let f = BufReader::new(f);
        let mut command = false;
    
        // We iterate through lines and enumerate them. Then we can make line as string
        // and look for user given command name.
        for (_num, line) in f.lines().enumerate() {
            let l = line.unwrap();
            
            let cmd: Command = serde_json::from_str(&l).unwrap();
            if cmd.command_name.contains(&args[1]) && command == false {
                println!("{}", "Command found from file".green());
                command = true;
                Command::exectue_command(&cmd).map_err(|err| println!("{:?}", err)).ok();
            }
        }

        Ok(())
    }

    fn list_commands() -> Result<(), io::Error> {
        let f = OpenOptions::new().read(true).open(".commands_frostmourne")?;
        let f = BufReader::new(f);

        for line in f.lines() {
            println!("{}", line.unwrap());
        }

        Ok(())
    }
}

pub fn call_command(args: &[String]) -> Result<(), ()> {

    // Here we start checking what is the first argument in the array
    // Help function which explains something about implemented commands
    if args.is_empty() || args[0] == "help" {
        println!("{}", "
            Commands available:
                - help
                    - This help command that you see now
                -new command_name PATH:path ARG:command_1 ARG:command_2 ARG:command_n -- UNDER CONSTRUCTION
                    - Creates new command and also new file .commands_frostmourne if doesn't exists
            To exit program execute ctr + c
        ".yellow());
    }

    else if args[0] == "getCommand" {
        Command::get_command(&args).unwrap();
        /*
        First we get command with name. After we got line, then we need to mark index, which is
        the order to exectue these commands. Like when we give PATH:/asd/das/esd ARG:command_1 PATH:/usd/ ARG:comman_2
        We need to know that second path change is after command_1. 
        */
    }

    else if args[0] == "list" {
         Command::list_commands().map_err(|err| println!("{:?}", err)).ok();
    }

    else if &args[0] == "new" {
        // let mut paths: Vec<String> = vec![];
        let mut arguments: Vec<String> = args.to_vec();
        for arg in arguments.iter_mut() {
            if arg.contains("ARG:") {
                let mut argument: String = arg.to_string();
                argument.replace_range(0..3, "");
                *arg = argument;
            }
        }
        // First parameter is always path rest are new commands
        let command_name_arg = args[1].clone();
        let new_command = Command::new(command_name_arg, &arguments.to_vec());
        let return_string = Command::create_command(&new_command);
        println!("Here is return string from create_command method: {:?}", return_string);
    }

    else {
        println!("{} {}", args[0], " <= There is no such command as first argument".red() );
    }

    Ok(())
}