use std::{process, env};
use crossbeam_channel::{ bounded, Receiver, select };
use exitfailure;

mod commands;

fn ctrl_channel() -> Result<Receiver<()>, ctrlc::Error> {
    // Here we just monitor for the ctr + c
    let (sender, receiver) = bounded(100);
    ctrlc::set_handler(move || {
        let _ = sender.send(());
    })?;

    Ok(receiver)
}

fn main() -> Result<(), exitfailure::ExitFailure> {
    // We get channel for ctrl + c, which we are going to monitor
    let ctrl_c_events = ctrl_channel()?;
    // We get arguments from command line and we filter the first launch argument and map rest to vector
    let args: Vec<String> = env::args().collect::<Vec<_>>().into_iter().enumerate().filter(|&(i, _)|i != 0).map(|(_, e)| e).collect::<Vec<_>>();
    // Then we make command variable which consists result and we kill process by matching this
    let command = commands::call_command(&args);

    // We loop until we get kill command by ctr + c, which is monitored all the time.
    // When we get ctr + c we also kill any commands going on started by frostmourne.
    loop {
        select! {
            recv(ctrl_c_events) -> _ => {
                process::exit(match command {
                    Ok(_) => 0,
                    Err(err) => {
                        eprintln!("error: {:?}", err);
                        1
                    }
                });
            }
        }
    }
}
